package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.Work;
import ru.og_dev.og_dev_ru.repository.WorkRepository;

import java.util.HashMap;

@Service
public class WorkStatisticService {
    public Integer todayMin;

    public HashMap<String, Double> todayMinByExec;

    @Autowired
    WorkRepository workRepository;
    public Iterable<Work> todayWorks;

    public WorkStatisticService getToday() {
        todayWorks = workRepository.findTodayActive();
        todayMinByExec = new HashMap<>();
        for (Work work : todayWorks) {
            Integer fact = work.getFact();
            String exec = work.getExecutor();
            if (fact == null || exec == null) continue;
            Double factHour = Math.round((work.getFact() / 60.0) * 100) / 100.0;
            Double hour = todayMinByExec.get(exec);
            if (hour == null) hour = factHour;
            else hour = hour + factHour;
            todayMinByExec.put(exec, hour);
        }

        return this;
    }

}
