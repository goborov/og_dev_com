package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.model.Work;
import ru.og_dev.og_dev_ru.repository.UserRepository;
import ru.og_dev.og_dev_ru.repository.WorkRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class WorkService {

    public Iterable<Work> works;
    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();
    @Autowired
    private WorkRepository workRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    private Iterable<User> users;
    private List<Work> worksReverse;
    private HashMap<String, Integer> minByCust;
    private HashMap<String, Integer> minByCustAndExec;

    public WorkService getForListPage(WorkFilterService filter) {


        User user = userService.getCurUser();
        if (user.getCustomer() != null) {
            System.out.println(user.getCustomer());
            if (!user.getCustomer().equals("*")) {
                filter.customer = user.getCustomer();
            }
        }

        works = workRepository.findAllActiveOrderByIdAscSortDesc(
            filter.customer,
            filter.project,
            filter.executor,
            filter.status,
            filter.from,
            filter.to
        );

        worksReverse = new ArrayList<>();
        users = userRepository.findAll();

        for (Work work : works) {
            setInHours(work);
            setMark(work);
            addToReverseList(work);
        }

        calcPayments();

        h1 = "Задачи";
        title = h1;
        desc = title;
        crumbs.put("#", title);

        return this;
    }

    private void setInHours(Work work) {
        if (work.getFact() == null) return;
        Double minutes = work.getFact() / 60.0;
        Double roundOff = Math.round(minutes * 100) / 100.0;
        work.setHour(roundOff);
    }

    private void setMark(Work work) {

        Date date = work.getDate();
        if (date == null) return;
        LocalDate dateWork = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dateToday = LocalDate.now(ZoneId.systemDefault());

        Integer fact = work.getFact();
        String status = work.getStatus();
        String customer = work.getCustomer();

        if (status.equals("Выполнено") && (fact == null || fact == 0)) work.setMark("error");
        else if (status.equals("В плане") && (fact != null && fact > 0)) work.setMark("error");
        else if (status.equals("В плане") && dateWork.isBefore(dateToday)) work.setMark("error");
        else if (customer == null || customer.equals("")) work.setMark("error");
        else if (work.getProject() == null) work.setMark("payment");
        else if (dateWork.isAfter(dateToday)) work.setMark("plan");
        else if (dateWork.isEqual(dateToday)) work.setMark("today");

    }

    private void addToReverseList(Work work) {
        worksReverse.add(0, work);
    }

    private void calcPayments() {

        minByCust = new HashMap<>();
        minByCustAndExec = new HashMap<>();

        for (Work work : worksReverse) {

            Integer fact = work.getFact();
            String cust = work.getCustomer();
            String custExec = cust + "" + work.getExecutor();

            if (work.getProject() == null) {

                String text = "";
                for (User user : users) {
                    Double hours = inHours(minByCustAndExec.get(cust + user.getInitials()));
                    if (hours > 0.0) text += ", " + user.getInitials() + ":" + hours;
                }
                work.setName("Расчетный период «" + cust + "», часов: " + inHours(minByCust.get(cust)) + text);

                // reset period
                minByCust.put(cust, 0);

                // reset by executors
                for (User user : users)
                    minByCustAndExec.put(cust + "" + user.getInitials(), 0);

            }

            Integer min = 0;
            if (fact != null) {

                // calc by project & customer
                if (work.getExecutor() != null) {
                    min = minByCustAndExec.get(custExec);
                    if (min == null) min = fact;
                    else min = min + fact;
                    minByCustAndExec.put(custExec, min);
                }

                // calc by project
                min = minByCust.get(cust);
                if (min == null) min = fact;
                else min = min + fact;
                minByCust.put(cust, min);
            }
        }
    }

    private Double inHours(Integer minutes) {
        if (minutes == null)
            return 0.0;
        return Math.round((minutes / 60.0) * 10) / 10.0;
    }

    public String getCSV(WorkService ob) {
        //String csv = "a,b,c\n1,2,3\n3,4,5";
        String csv = "";

        csv += "ID;Внешний ID;Задача;Прогноз;Факт;Час;Дата;" +
            "Заказчик;Проект;Постановщик;Исп.;Статус;Приоритет.\n";

        for (Work work : ob.works) {
            csv +=
                work.getId() + ";" +
                    (work.getExt_id() != null ? work.getExt_id() : "-") + ";" +
                    (work.getName() != null ? work.getName() : "-") + ";" +
                    (work.getPrediction() != null ? work.getPrediction() : "-") + ";" +
                    (work.getFact() != null ? work.getFact() : "-") + ";" +
                    (work.getHour() != null ? work.getHour() : "-") + ";" +
                    (work.getDate() != null ? work.getDate() : "-") + ";" +
                    (work.getCustomer() != null ? work.getCustomer() : "-") + ";" +
                    (work.getProject() != null ? work.getProject() : "-") + ";" +
                    (work.getProvider() != null ? work.getProvider() : "-") + ";" +
                    (work.getExecutor() != null ? work.getExecutor() : "-") + ";" +
                    (work.getStatus() != null ? work.getStatus() : "-") + ";" +
                    (work.getPriority() != null ? work.getPriority() : "-") + ";" +
                    "\n";
        }


        return csv;
    }

}
