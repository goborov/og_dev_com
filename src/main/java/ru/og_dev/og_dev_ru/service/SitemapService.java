package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.Constants;
import ru.og_dev.og_dev_ru.model.Article;
import ru.og_dev.og_dev_ru.repository.ArticleRepository;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SitemapService implements Constants {

    @Autowired
    private ArticleRepository articleRepository;

    private String sitemap, lastmod;

    private final List<String> staticPages = new ArrayList<>(
        Arrays.asList("", "/articles", "/resume", "/contacts"));

    public String getSiteMap() {
        getXmlDate();
        setXmlHeader();
        addStatic();
        addArticles();
        setXmlFooter();
        return sitemap;
    }

    private void getXmlDate() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormatted = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormatted = new SimpleDateFormat("hh:mm:ss");
        String nowDate = dateFormatted.format(new Date());
        String nowTime = timeFormatted.format(new Date());
        lastmod = nowDate + "T" + nowTime + "+03:00";
    }

    private void setXmlHeader() {
        sitemap = "<?xml version='1.0' encoding='UTF-8'?>" +
            "\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";
    }

    private void addToXml(String url) {
        sitemap += "\n<url>" +
            "\n\t<loc>" + PROTOCOL + "://" + DOMAIN + url + "</loc>" +
            "\n\t<lastmod>" + lastmod + "</lastmod>" +
            "\n\t<changefreq>always</changefreq>" +
            "\n\t<priority>1.0</priority>" +
            "\n</url>";
    }

    private void addStatic() {
        for (String page : staticPages)
            addToXml(page);
    }

    private void addArticles() {
        Iterable<Article> articles = articleRepository.findAll();
        for (Article article : articles)
            addToXml("/articles/" + article.getCode());
    }

    private void setXmlFooter() {
        sitemap += "\n</urlset>";
    }
}
