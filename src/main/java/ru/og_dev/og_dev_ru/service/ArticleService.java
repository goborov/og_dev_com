package ru.og_dev.og_dev_ru.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.og_dev.og_dev_ru.model.Article;
import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.repository.ArticleRepository;

import java.util.HashMap;


@Service
public class ArticleService {
    public Article article;
    public String title, h1, desc;
    public HashMap<String, String> crumbs = new HashMap<>();
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserService userService;
    public Integer userPerm;

    public ArticleService get(String code) {
        article = articleRepository.findByCode(code).iterator().next();

        updateShowCounter(article);

        User user = userService.getCurUser();
        if (user != null) userPerm = user.getPermission();
        else userPerm = 0;

        h1 = article.getName();
        title = "Записки, шпаргалки, " + article.getName();
        desc = title;
        crumbs.put("/articles/", "Записки");
        crumbs.put("#", title);

        return this;
    }

    public ArticleService editPage(Long id) {
        article = articleRepository.findById(id).get();

        title = "Редактирование " + article.getName();
        h1 = title;
        desc = title;
        crumbs.put("/articles/", "Записки");
        crumbs.put("#", title);

        return this;
    }

    public String save(Article articleData) {

        Article article = articleRepository.findById(articleData.getId()).get();
        article.setDetail_text(articleData.getDetail_text());
        article.setPreview_text(articleData.getPreview_text());
        articleRepository.save(article);

        return article.getCode();
    }

    private void updateShowCounter(Article article) {
        Integer cnt = article.getShow_counter();
        article.setShow_counter(++cnt);
        articleRepository.save(article);
    }


}
