package ru.og_dev.og_dev_ru.repository;

import ru.og_dev.og_dev_ru.model.Project;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Integer> {

  public Iterable<Project> findAllByOrderByCodeAsc();

  @Modifying
  @Query(
    value = "SELECT * FROM projects ORDER BY code ASC",
    nativeQuery = true
  )
  Iterable<Project> findAll();
}
