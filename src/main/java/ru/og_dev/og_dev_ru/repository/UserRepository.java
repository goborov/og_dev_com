package ru.og_dev.og_dev_ru.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.og_dev.og_dev_ru.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    //User findByEmail(String email);

    @Query("SELECT u FROM User u " +
        "WHERE u.permission > 7 " +
        "ORDER BY u.initials ASC "
    )
    Iterable<User> getExecutors();

    //List<User> findAll();

    @Modifying
    @Query(
        value = "SELECT * FROM users u WHERE u.email= ?1 LIMIT 1",
        nativeQuery = true
    )
    Iterable<User> findByEmail(String email);

}
