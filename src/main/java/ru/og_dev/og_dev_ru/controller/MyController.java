package ru.og_dev.og_dev_ru.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MyController {
  @GetMapping("/my")
  public void get(HttpServletResponse httpServletResponse) throws IOException {
    httpServletResponse.sendRedirect("/my/works");
  }
}