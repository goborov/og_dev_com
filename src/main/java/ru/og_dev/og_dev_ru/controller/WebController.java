package ru.og_dev.og_dev_ru.controller;

import ru.og_dev.og_dev_ru.model.User;
import ru.og_dev.og_dev_ru.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WebController extends BasicController {

    @Autowired
    private UserRepository repo;

    @GetMapping("/")
    String getAn(Model model) {
        model.addAttribute("h1", "Main page");
        return "main";
    }

    /*@GetMapping("/contacts")
    String getRoute(Model model, HttpServletRequest request) {
      model.addAttribute("h1", "Contacts page");
      setTheme(model, request);
      return "main";
    }*/

    @GetMapping("/list")
    public String showUserList(Model model, HttpServletRequest request) {

        Iterable<User> listUser = repo.findAll();
        model.addAttribute("h1", "User list:");
        model.addAttribute("users", listUser);
        setTheme(model, request);
        return "main";
    }

}
