package ru.og_dev.og_dev_ru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OGDevRuApplication {

  public static void main(String[] args) {

    SpringApplication.run(OGDevRuApplication.class, args);

  }

}
